(() => {
  /**
   * Handle highlighting the observed object.
   *
   * @param {IntersectionObserverEntry} entries  - The entries being observed.
   * @param {IntersectionObserver}      observer - The observer.
   */
  function highlight(entries, observer) {
    // There is always an array of entries passed, so we need to loop through them.
    entries.forEach(entry => {
      // First we need to check if the entry is truely intesecting.
      // If it is, then we can add our class to animate the text highlight.
      if (entry.isIntersecting) {
        setTimeout(() => {
          entry.target.classList.add("active");
        }, 150);

        // Once our animation has happened, we can stop observing this element
        // to save resources on the client's end. It's best practice to do this.
        observer.unobserve(entry.target);
      }
    })
  }

  // Observer options.
  const options = {
    rootMargin: "0px",
    threshold: 0.1,
  };

  // Observer.
  const observer = new IntersectionObserver(highlight, options);

  window.addEventListener("DOMContentLoaded", () => {
    const elements = Array.from(document.querySelectorAll("mark"));

    elements.forEach(element => {
      // Set a custom animation length based on the length of text.
      const highlightDuration = 5;
      const animationLength = element.innerHTML.length * highlightDuration;
      const reducedMotion = window.matchMedia("(prefers-reduced-motion: reduce)");

      if (!reducedMotion.matches) {
        element.style.transition = `background-size ${animationLength}ms ease-in`;

        // Observe element.
        observer.observe(element);
      }
    });

  });
})();
